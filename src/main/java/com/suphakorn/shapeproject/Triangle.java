/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.shapeproject;

/**
 *
 * @author exhau
 */
public class Triangle {
    private double b;
    private double h;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return 1.0/2.0*b*h;
    }
    public double getB(){
        return b;
    }
    public double getH(){
        return h;
    }
     public void setBH(double b,double h){
        if(b <= 0 || h <= 0){
            System.out.println("Error : Base or Height must more than zero!!!!");
            return;
        }
        this.b = b;
        this.h = h;
    }
}
