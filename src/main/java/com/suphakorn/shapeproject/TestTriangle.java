/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.shapeproject;

/**
 *
 * @author exhau
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(4,3);
        System.out.println("Area of triangle1(b = " + triangle1.getB() +" ,h = " + triangle1.getH() + ") is " + triangle1.calArea());
        triangle1.setBH(3,2);
        System.out.println("Area of triangle1(b = " + triangle1.getB() +" ,h = " + triangle1.getH() + ") is " + triangle1.calArea());
        triangle1.setBH(0,0);
        System.out.println("Area of triangle1(b = " + triangle1.getB() +" ,h = " + triangle1.getH() + ") is " + triangle1.calArea());
    }
}
